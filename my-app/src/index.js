import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  return (
    <button className={"square " + (props.value? props.value: "")} onClick={props.onClick}>      
    </button>
  );
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    var positions = placeEnemies(this.props.width, this.props.height);
    //var squares = Array(this.props.width * this.props.height).fill(null);
    var squares = new Array(this.props.height);
    for(let i = 0; i < this.props.height; i++) {
      squares[i] = new Array(this.props.width);
    }
    for(let i =0; i < positions.length; i++) {
      squares[positions[i][0]][positions[i][1]] = "enemy";
    }
    squares[0][0] = "hero";
    this.state = {
      squares: squares,      
      noOfEnemies: positions.length,
      current: [0, 0],
    };
  }
  handleKeyDown(event) {
    const squares = this.state.squares.slice();
    let current = this.state.current;
    let last = current.slice();

    let noOfEnemies = this.state.noOfEnemies;

    if(event.keyCode == 38) {
      // up
      if ( current[0] > 0) {
        current[0] = current[0] - 1;
      }
    }
    if(event.keyCode == 40) {
      // down
      if ( current[0] < this.props.height - 1) {
        current[0] = current[0] + 1;        
      }
    }
    if(event.keyCode == 37) {
      // left
      if (current[1] > 0) 
        current[1] = current[1] - 1;
    }
    if(event.keyCode == 39) {
      // right
      if (current[1] < this.props.width - 1) 
        current[1] = current[1] + 1;
    }
    
    if (squares[current[0]][current[1]] === "enemy") {
      noOfEnemies--;
    }
    squares[last[0]][last[1]] = "";
    squares[current[0]][current[1]] = "hero";
    if(noOfEnemies == 0) {
      alert("You won the game!");
    }
    this.setState({squares: squares, current: current, noOfEnemies: noOfEnemies});
  }
  renderSquare(i, j) {
    return (
            <Square
              value={this.state.squares[i][j]}
            />
    );
  }

  render() {
    const status = 'Next player: X';
    var rows = [];
    var k = 0;
    for(let i = 0; i < this.props.height; i++) {
      var innerRows = [];
      for(let j= 0; j < this.props.width; j++) {
        innerRows.push(this.renderSquare(i, j));
        k++;
      }
      rows.push(<div classname="board-row">{innerRows}</div>);
    }   
    return (
      <div onKeyDown={this.handleKeyDown.bind(this)}>
        {rows}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 10,
      height: 10,
    };
  }
  render() {
    return (
      <div className="game">
        <div className="game-board">          
          <Board 
            width={this.state.width = prompt("Board width:")}
            height={this.state.height = prompt("Board height:")}  
          />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function placeEnemies(width, height) {
  let noOfPositions = width*height;
  // Between 1 and noOfPositions - 1
  let noOfEnemies = Math.floor((Math.random() * (noOfPositions - 1)) + 1);

  let positions = [];
  for(let i=0; i < noOfEnemies; i++) {
    // Between 1 and noOfPositions - 1
    let a = Math.floor(Math.random() * (height - 1));
    let b = Math.floor(Math.random() * (width - 1));
    let flag = true;
    
    for(let j=0; j < positions.length; j++) {
      if(positions[j][0] == a && positions[j][1] == b) {
        flag = false;
        break;
      }
    }

    if (a == 0 && b == 0)
      flag = false;

    if (flag)
      positions.push([a, b]);
    else
      i--;
  }
  return positions;
}